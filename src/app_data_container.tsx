import {ApplicationData} from "./types";
import AppHeader from "./app_header";
import TotalStatsSection from "./total_stats_section";
import TodayStatsSection from "./today_stats_section";

interface AppDataContainerProps {
  appData: ApplicationData;
}

export default function AppDataContainer(props: AppDataContainerProps) {
  const { appData } = props;

  return <>
    <AppHeader totalFollowers={appData.totalFollowers} />
    <TotalStatsSection data={appData.totalStats} />
    <TodayStatsSection data={appData.todayStats} />
  </>;
}
