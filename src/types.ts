import {camelCase} from "lodash";

export type NumberOrPercentage = number | string;

export interface TotalCardData {
  mediaName: string;
  profileName: string;
  followers: number;
  followersText: string;
  change: NumberOrPercentage;
}

export interface TodayCardData {
  mediaName: string;
  statName: string;
  value: number;
  change: NumberOrPercentage;
}

export interface ApplicationData {
  totalFollowers: number;
  totalStats: TotalCardData[];
  todayStats: TodayCardData[];
}

type ApplicationDataOrNull = ApplicationData | null;
type UnaryFunction<T, R> = (arg: T) => R;

function modifyObjectKeys(obj: any, rule: UnaryFunction<any, any>): any {
  if (typeof obj !== 'object' || obj === null) {
    return obj;
  }

  if (Array.isArray(obj)) {
    return obj.map(item => modifyObjectKeys(item, rule));
  }

  const newObj: any = {};

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      const newKey = rule(key);
      newObj[newKey] = modifyObjectKeys(obj[key], rule);
    }
  }

  return newObj;
}

export function adoptApplicationData(json: any): ApplicationDataOrNull {
  return modifyObjectKeys(json, item => camelCase(item)) as ApplicationData;
}

interface Unit {
  divisor: number;
  limit: number;
  suffix: string;
}

function convertToSuffixWithUnit(value: number, unit: Unit): string {
  return `${Math.floor(value / unit.divisor)}${unit.suffix}`;
}

function convertToSuffix(value: number): string {
  const units: Unit[] = [
    { divisor: 1_000_000_000, limit: 10_000_000_000, suffix: 'G' },
    { divisor: 1_000_000, limit: 10_000_000, suffix: 'M' },
    { divisor: 1_000, limit: 10_000, suffix: 'k' },
  ];

  const foundUnit = units.find((unit: Unit) => value >= unit.limit);
  return foundUnit !== undefined ? convertToSuffixWithUnit(value, foundUnit) : value.toString();
}

type NullableBoolean = boolean | null;

export interface StringWithTriState {
  str: string;
  state: NullableBoolean;
}

function numberSignToTriState(value: number): NullableBoolean {
  return value !== 0 ? value > 0 : null;
}

export function beautifyNumberOrPercentage(value: NumberOrPercentage): StringWithTriState {
  switch (typeof value) {
    // Percent
    case 'string':
      return { str: value[0] === '-' ? value.slice(1) : value, state: numberSignToTriState(parseInt(value)) };
    case 'number':
      return { str: convertToSuffix(Math.abs(value)), state: numberSignToTriState(value) };
    default:
      throw new Error("Invalid value");
  }
}
