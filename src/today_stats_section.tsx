import {TodayCardData} from "./types";
import TodayStatsCard from "./today_stats_card";
import clsx from "clsx";

interface TodayStatsSectionProps {
  data: TodayCardData[];
}

export default function TodayStatsSection(props: TodayStatsSectionProps) {
  const todayStatsSectionClass = clsx('today_stats_section');
  const titleClass = clsx('title');
  const todayStatsCardsClass = clsx('today_stats_cards');
  const { data } = props;

  return <>
    <section className={todayStatsSectionClass}>
      <p className={titleClass}>Overview - Today</p>
      <div className={todayStatsCardsClass}>
        {data.map((cardData, index) =>
          <TodayStatsCard key={index} cardData={cardData} />
        )}
      </div>
    </section>
  </>;
}
