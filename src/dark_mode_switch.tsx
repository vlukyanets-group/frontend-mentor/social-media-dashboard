import React, {useContext} from "react";

type DarkModeContextType = [boolean, (value: boolean) => void];

export const DarkModeContext = React.createContext<DarkModeContextType>(
  [true, () => {}]);

export default function DarkModeSwitch() {
  const [isDark, setIsDark] = useContext(DarkModeContext);

  return (
    <div
      className={'dark-switch' + (isDark ? " active" : "")}
      onClick={() => setIsDark(!isDark)}
    >
      <span>Dark Mode</span>
      <div className='switch'>
        <div className='switch-point'></div>
      </div>
    </div>
  )
}
