import DarkModeSwitch from "./dark_mode_switch";
import clsx from "clsx";

interface AppHeaderProps {
  totalFollowers: number;
}

export default function AppHeader(props: AppHeaderProps) {
  const appHeaderClass = clsx('app_header');
  const appHeaderLeftClass = clsx('app_header_left');
  const titleClass = clsx('title');
  const infoClass = clsx('info');
  const { totalFollowers } = props;

  return <>
    <div className={appHeaderClass}>
      <div className={appHeaderLeftClass}>
        <p className={titleClass}>Social Media Dashboard</p>
        <p className={infoClass}>Total Followers: {totalFollowers.toLocaleString()}</p>
      </div>
      <hr/>
      <DarkModeSwitch />
    </div>
  </>;
}
