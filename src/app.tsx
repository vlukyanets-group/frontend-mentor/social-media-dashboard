import "./app.css";
import AppDataContainer from "./app_data_container";
import {DarkModeContext} from "./dark_mode_switch";
import {adoptApplicationData, ApplicationData} from "./types";
import {useEffect, useState} from "react";
import clsx from "clsx";

export default function App() {
  const [appData, setAppData] = useState<ApplicationData | null>(null);
  const [isDark, setIsDark] = useState(true);

  useEffect(() => {
    const abortController = new AbortController();
    const signal = abortController.signal;

    const fetchData = async () => {
      try {
        const response = await fetch(`/data.json`, { signal });
        setAppData(adoptApplicationData(await response.json()));
      } catch (error) {
        console.log('Error fetching JSON data:', error)
      }
    };

    fetchData();

    return () => {
      abortController.abort();
    }
  }, []);

  const appClass = clsx('app', isDark ? 'dark' : 'light');
  const appPaddingClass = clsx('app_padding');

  return <>
    <DarkModeContext.Provider value={[isDark, setIsDark]}>
      <div className={appClass}>
        <div className={appPaddingClass}>
          {appData && <AppDataContainer appData={appData} />}
        </div>
      </div>
    </DarkModeContext.Provider>
  </>;
}
