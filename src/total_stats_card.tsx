import {TotalCardData, beautifyNumberOrPercentage} from "./types";
import ChangeShow from "./change_show";
import Icon from "./icon";
import clsx from "clsx";

interface TotalStatsCardProps {
  cardData: TotalCardData;
}

export default function TotalStatsCard(props: TotalStatsCardProps) {
  const { cardData } = props;
  const totalStatsCardClass = clsx('total_stats_card', 'card', cardData.mediaName);
  const profileClass = clsx('profile');
  const profileNameClass = clsx('profile_name');
  const followersCounterClass = clsx('followers_counter');
  const followersTextClass = clsx('followers_text');
  const beautifiedFollowers = beautifyNumberOrPercentage(cardData.followers);

  return <>
    <div className={totalStatsCardClass}>
      <div className={profileClass}>
        <Icon name={cardData.mediaName} />
        <p className={profileNameClass}>{cardData.profileName}</p>
      </div>
      <div className={followersCounterClass}>{beautifiedFollowers.str}</div>
      <div className={followersTextClass}>{cardData.followersText}</div>
      <ChangeShow changeData={cardData.change} text={' Today'} />
    </div>
  </>;
}
