import {beautifyNumberOrPercentage, NumberOrPercentage} from "./types";
import Icon from "./icon";
import clsx from "clsx";

interface ChangeShowProps {
  changeData: NumberOrPercentage;
  text?: string;
}

export default function ChangeShow(props: ChangeShowProps) {
  const { changeData, text } = props;
  const endingText = (text === undefined) ? "" : text;
  const beautifiedChange = beautifyNumberOrPercentage(changeData);
  const up = beautifiedChange.state === true, down = beautifiedChange.state === false;
  const changeShowClass = clsx(
    'change_show',
    {
      'up': beautifiedChange.state === true,
      'down': beautifiedChange.state === false,
      // Third state has no class name
    }
  );

  return <>
    <div className={changeShowClass}>
      {(up || down) && <Icon name={up ? "up" : "down"} />}
      {`${beautifiedChange.str}${endingText}`}
    </div>
  </>;
}
