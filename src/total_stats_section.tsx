import {TotalCardData} from "./types";
import TotalStatsCard from "./total_stats_card";
import clsx from "clsx";

interface TotalStatsSectionProps {
  data: TotalCardData[];
}

export default function TotalStatsSection(props: TotalStatsSectionProps) {
  const totalStatsSectionClass = clsx('total_stats_section');
  const { data } = props;

  return <section className={totalStatsSectionClass}>
    {data.map((cardData, index) =>
      <TotalStatsCard key={index} cardData={cardData} />
    )}
  </section>;
}
