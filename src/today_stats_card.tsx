import {beautifyNumberOrPercentage, TodayCardData} from "./types";
import ChangeShow from "./change_show";
import Icon from "./icon";
import clsx from "clsx";

interface TodayCardDataProps {
  cardData: TodayCardData;
}

export default function TodayStatsCard(props: TodayCardDataProps) {
  const { cardData } = props;
  const todayStatsCardClass = clsx('today_stats_card', 'card', cardData.mediaName);
  const todayStatsCardContentClass = clsx('today_stats_card_content');
  const statNameClass = clsx('stat_name');
  const valueClass = clsx('value');
  const beautifiedValue = beautifyNumberOrPercentage(cardData.value);

  return <>
    <div className={todayStatsCardClass}>
      <div className={todayStatsCardContentClass}>
        <span className={statNameClass}>{cardData.statName}</span>
        <Icon name={cardData.mediaName} />
        <span className={valueClass}>{beautifiedValue.str}</span>
        <ChangeShow changeData={cardData.change} />
      </div>
    </div>
  </>;
}
