import clsx from "clsx";

interface IconProps {
  name: string;
}

export default function Icon(props: IconProps) {
  const iconClass = clsx('icon');

  return <>
    <img className={iconClass} src={`/icon-${props.name}.svg`} alt={props.name} />
  </>;
}
